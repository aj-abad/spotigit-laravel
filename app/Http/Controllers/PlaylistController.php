<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Playlist;
use App\Models\User;
use App\Models\Commit;
use Illuminate\Support\Facades\Validator;
use \Httpful\Request as HttpRequest;

class PlaylistController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }


  public function index()
  {
    $user_id = auth()->user()->id;
    $playlists = Playlist::where('user_id', $user_id)->get();
    return $playlists;
  }

  public function details($id)
  {
    $playlist = Playlist::find($id);
    $playlist->commits = $playlist->commits()->get();
    $playlist->last_commit = $playlist->commits()->orderBy('created_at', 'desc')->first();
    $playlist->username = User::find($playlist->user_id)->username;
    if ($playlist->last_commit) {
      $contents = $playlist->last_commit->contents;
      $song_ids =  implode(",", $contents);
      //get song details
      $token = env("SPOTIFY_TOKEN");
      $url = "https://api.spotify.com/v1/tracks?ids=$song_ids";
      $response = HttpRequest::get($url)
        ->addHeaders(["Authorization" => "Bearer $token"])
        ->send();
      $playlist->songs = $response->body;
    }
    return $playlist;
  }

  public function create(Request $request)
  {

    $name = $request->input('name');
    $description = $request->input('description') ?? "";
    $user_id = auth()->user()["id"];

    $validator = Validator::make($request->all(), [
      'name' => 'required|max:255',
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors(), 400);
    }

    $new_playlist = new Playlist(
      [
        'name' => $name,
        'description' => $description,
        'user_id' => $user_id
      ]
    );

    $new_playlist->save();
    return response()->json($new_playlist, 201);
  }




  public function commit(Request $request)
  {
    $playlist_id = $request->input('id');
    $user_id = auth()->user()["id"];
    $commit_message = $request->input('commitMessage');
    $added_songs = $request->input('addedSongs');
    $deleted_songs = $request->input('deletedSongs');





    $playlist = Playlist::find($playlist_id);
    if (!$playlist) {
      return response()->json(['message' => 'Playlist not found'], 404);
    }
    if ($playlist->user_id != $user_id) {
      return response()->json(['error' => 'Unauthorized'], 401);
    }

    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:playlists,id',
      'commitMessage' => 'required|max:255',
      'addedSongs' => 'array',
      'deletedSongs' => 'array',
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors(), 400);
    }

    //Get last commit
    $last_commit = $playlist->commits()->orderBy('created_at', 'desc')->first();
    if ($last_commit) {
      //copy contents array
      $contents = $last_commit->contents;
      //parse contents
      //delete from contents
      foreach ($deleted_songs as $song) {
        $key = array_search($song, $contents);
        if ($key !== false) {
          unset($contents[$key]);
        }
      }
      //add to contents
      foreach ($added_songs as $song) {
        $contents[] = $song;
      }
      //encode contents
      $new_commit = new Commit(
        [
          'commit_message' => $commit_message,
          'contents' => $contents,
          'playlist_id' => $playlist_id
        ]
      );
      $new_commit->save();
      return response()->json($new_commit, 201);
    }

    //first commit
    $new_commit = new Commit(
      [
        'commit_message' => $commit_message,
        'contents' => $added_songs,
        'playlist_id' => $playlist_id
      ]
    );
    $new_commit->save();
    return response()->json($new_commit, 201);
  }


  public function export(Request $request)
  {
    $name = $request->input('name');
    $description = $request->input('description') ?? "";
    $playlist_id = $request->input('id');
    $user_id = auth()->user()["id"];

    //make spotify playlist
    $token = env("SPOTIFY_TOKEN");
    $url = "https://api.spotify.com/v1/users/12142893601/playlists";

    $response = HttpRequest::post($url)
      ->addHeaders(["Authorization" => "Bearer $token"])
      ->body(json_encode(['name' => $name, 'description' => $description, 'public' => true]))
      ->send();
    //get playlist link
    $playlist_link = $response->body->external_urls->spotify;
    //Add songs to created playlist
    $songs = Playlist::find($playlist_id)->commits()->orderBy('created_at', 'desc')->first()->contents;


    //add prefix to songs
    foreach ($songs as $key => $song) {
      $songs[$key] = "spotify:track:" . $song;
    }

    $playlist_id = $response->body->id;
    $token = env("SPOTIFY_TOKEN");
    $url = "https://api.spotify.com/v1/playlists/$playlist_id/tracks";
    $response = HttpRequest::post($url)
      ->addHeaders(["Authorization" => "Bearer $token"])
      ->body(json_encode(['uris' => $songs]))
      ->send();

    //return link to playlist
    return $playlist_link;
  }
  public function delete(Request $request)
  {
    $id = $request->input('id');
    $user_id = auth()->user()["id"];
    $playlist = Playlist::find($id);
    if (!$playlist) {
      return response()->json(['message' => 'Playlist not found'], 404);
    }
    if ($playlist->user_id != $user_id) {
      return response()->json(['error' => 'Unauthorized'], 401);
    }
    $playlist->delete();
    return response()->json(null, 204);
  }
}
