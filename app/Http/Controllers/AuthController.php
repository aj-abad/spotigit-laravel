<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserVerification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api")->except(["signIn", "signUp"]);
    }

    public function signIn(Request $request)
    {
        $credentials = request(['username', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'These credentials don\'t match our records.'], 401);
        }
        return response()->json([
            'token' => $token,
            'user' => auth()->user(),
        ]);
    }

    public function me(Request $request)
    {
        return auth()->user();
    }

    public function signUp(Request $request)
    {
        $username = $request->input("username");
        $password = $request->input("password");

        $validator = Validator::make(
            [
                "username" => $username,
                "password" => $password,
            ],
            [
                "username" => "required|unique:users|alpha_dash",
                "password" => "bail|required|min:6",
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $new_user =  User::create([
            "username" => $username,
            "password" => $password,
            "status" => 1
        ]);

        return response()->json([
            'token' => auth()->attempt(["username" => $username, "password" => $password]),
            'user' => $new_user
        ]);
    }
}
