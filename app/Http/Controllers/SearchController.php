<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Playlist;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->input('q');

        //search users case insensitive
        $users = User::where('username', 'LIKE', "%{$keyword}%")->get();
        //search playlists case insensitive
        $playlists = Playlist::where('name', 'LIKE', "%{$keyword}%")->get();
        //get playlist username
        $playlists->map(function ($playlist) {
            $playlist->username = User::find($playlist->user_id)->username;
        });

        return response()->json([
            "keyword" => $keyword,
            'users' => $users,
            'playlists' => $playlists
        ]);
    }
}
