<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Httpful\Request as HttpRequest;

class TracksController extends Controller
{
    public function search(Request $request)
    {
        $keyword = $request->input("keyword");
        //html encode spaces
        $keyword = str_replace(" ", "%20", $keyword);

        //get token from env
        $token = env("SPOTIFY_TOKEN");
        $url  = "https://api.spotify.com/v1/search?query=$keyword&type=track&locale=en-US%2Cen%3Bq%3D0.9&offset=0&limit=10";
        $response = HttpRequest::get($url)
            ->addHeaders(["Authorization" => "Bearer $token"])
            ->send();
        return $response;
    }
}
