<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commit extends Model
{
    use HasFactory;

    //decode json on fetch
    protected static function booted()
    {
        static::retrieved(function ($commit) {
            $commit->contents = explode(",", trim($commit->contents));
        });
        static::creating(function ($commit) {
            $commit->contents = implode(",", array_filter($commit->contents));
        });
    }



    protected $fillable = [
        'commit_message',
        'contents',
        'playlist_id'
    ];
}
