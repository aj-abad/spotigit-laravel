<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    use HasFactory;
    public function commits()
    {
        return $this->hasMany('App\Models\Commit');
    }

    protected $fillable = [
        'name', 'description', 'user_id'
    ];
}
