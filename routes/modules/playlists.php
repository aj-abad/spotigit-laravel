<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\PlaylistController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix("playlists")->group(function () {
  Route::get("/", [PlaylistController::class, "index"]);
  Route::get("/{id}", [PlaylistController::class, "details"]);
  Route::post("/create", [PlaylistController::class, "create"]);
  Route::post("/commit", [PlaylistController::class, "commit"]);
  Route::post("/export", [PlaylistController::class, "export"]);
  Route::delete("/delete", [PlaylistController::class, "delete"]);
});
